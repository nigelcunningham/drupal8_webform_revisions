<?php

namespace Drupal\webform_revisions\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Entity\Webform;
use Drupal\webform_revisions\Entity\WebformRevisions;

/**
 * Provides a form for reverting a WebformRevisions revision.
 *
 * @internal
 */
class WebformRevisionsDeleteForm extends ConfirmFormBase {

  /**
   * The webform.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  protected $webform;

  /**
   * The WebformRevisions revision.
   *
   * @var \Drupal\WebformRevisions\WebformRevisionsInterface
   */
  protected $revision;

  /**
   * The WebformRevisions storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $WebformRevisionsStorage;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new WebformRevisionsRevisionDeleteForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $WebformRevisions_storage
   *   The WebformRevisions storage.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityStorageInterface $WebformRevisions_storage, Connection $connection) {
    $this->WebformRevisionsStorage = $WebformRevisions_storage;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity.manager');
    return new static(
      $entity_manager->getStorage('webform_revisions'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'WebformRevisions_revision_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to delete the revision from %revision-date?', ['%revision-date' => format_date($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.webform_revisions.revisions', ['webform' => $this->webform->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webform $webform = NULL, $webform_revision = NULL) {
    $this->webform = $webform;
    $this->revision = $this->WebformRevisionsStorage->loadRevision($webform_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->WebformRevisionsStorage->deleteRevision($this->revision->getRevisionId());

    $this->logger('webform')->notice('Deleted %form revision %revision.', ['%form' => $this->webform->label(), '%revision' => $this->revision->getRevisionId()]);
    drupal_set_message(t('Revision from %revision-date of %form has been deleted.', ['%revision-date' => format_date($this->revision->getRevisionCreationTime()), '%form' => $this->webform->label()]));
    $form_state->setRedirect(
      'entity.webform.edit_form',
      ['webform' => $this->webform->id()]
    );
    $webform_revisions_id = $this->webform->getThirdPartySetting('webform_revisions', 'contentEntity_id');
    if ($this->connection->query('SELECT COUNT(DISTINCT form) FROM {webform_revision_revision} WHERE id = :id', [':id' => $webform_revisions_id])->fetchField() > 1) {
      $form_state->setRedirect(
        'entity.webform_revisions.revisions',
        ['webform' => $this->webform->id()]
      );
    }
  }

}
