<?php

namespace Drupal\webform_revisions;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;

/**
 * Provides an interface defining a webform revisions entity.
 */
interface WebformRevisionsInterface extends ContentEntityInterface, EntityChangedInterface, RevisionLogInterface {

  /**
   * Returns the creation timestamp.
   *
   * @return int
   *   Creation timestamp of the webform revisions entity.
   */
  public function getCreatedTime();

  /**
   * Sets the webform_revisions creation timestamp.
   *
   * @param int $timestamp
   *   The webform_revision creation timestamp.
   *
   * @return \Drupal\webform_revisions\WebformRevisionsInterface
   *   The called webform_revision entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the webform_revision publisher user entity.
   *
   * @return \Drupal\user\UserInterface
   *   The author user entity.
   */
  public function getPublisher();

  /**
   * Returns the webform_revision publisher user ID.
   *
   * @return int
   *   The author user ID.
   */
  public function getPublisherId();

  /**
   * Sets the webform_revision publisher user ID.
   *
   * @param int $uid
   *   The author user id.
   *
   * @return \Drupal\webform_revisions\WebformRevisionsInterface
   *   The called webform_revision entity.
   */
  public function setPublisherId($uid);

  /**
   * Returns the webform_revision type.
   *
   * @return \Drupal\webform_revisions\WebformRevisionTypeInterface
   *   The webform_revision type.
   */
  public function getType();

}
