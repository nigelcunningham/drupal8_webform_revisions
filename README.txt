Webform Workflow
================

This module provides integration between the Drupal 8.5 version of webforms
and Drupal's core revision support, together work workflows.

Technical explanation
---------------------

Since webforms are configuration entities now, and they don't support
revisions or workflows, a custom content entity is used to provide the
integration between webforms and the revisions/workflows support. This
content entity stores serialised versions of the webform config entity
and updates the config entity as different revisions are published.

TODO List
---------

(Last updated 2 July 2018)
- Enable addition of revisions
- Enable update of revision ID when new version is saved
- Complete display of revisions
- Enable choosing a revision to be published from revisions list
- Enable editing of a unpublished revision
- Get local task working
- Investigate workflow states and learn what needs to be done there
- Complete README
