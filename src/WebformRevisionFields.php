<?php

namespace Drupal\webform_revisions;

use Drupal\webform\Entity\Webform;
use Drupal\webform_revisions\Entity\WebformRevisions;
use Drupal\webform_revisions\EntityRevisionsInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

class WebformRevisionFields  {
  use StringTranslationTrait;

  /**
   * @var Webform
   *   The webform for which a revision might be added.
   */
  private $webform;

  /**
   * @var WebformRevisions
   *   The webform_revisions object to which a revision might be added.
   */
  private $webform_revisions;

  /**
   * WebformContentEntityForm constructor.
   *
   * @param Webform $webform
   *   The webform to which the revision might be added.
   */
  public function __construct(Webform $webform) {
    $this->webform = $webform;

    $contentID = $webform->getThirdPartySetting(
      'webform_revisions', 'contentEntity_id');

    $revisionsEntityStorage = \Drupal::entityTypeManager()
      ->getStorage('webform_revisions');

    if ($contentID) {
      $this->webform_revisions = $revisionsEntityStorage->load($contentID);
    }
  }

  /**
   * Should new revisions created by default?
   *
   * @return bool
   *   No new revision by default.
   */
  public function getNewRevisionDefault() {
    return FALSE;
  }

  /**
   * Adds the revision form fields to the form.
   *
   * @param &$form
   *   The form to which the fields should be added.
   */
  public function addRevisionFormFields(&$form) {
    // Add a log field if the "Create new revision" option is checked, or if the
    // current user has the ability to check that option.
    $new_revision_default = self::getNewRevisionDefault();

    if (!isset($form['advanced'])) {
      $form['advanced'] = [
        '#type' => 'vertical_tabs',
        '#weight' => 99,
      ];
    }

    $form['advanced']['revisions'] = [
      '#title' => 'Revisions',
      '#type' => 'details',
      '#group' => 'advanced',
      '#weight' => 99,
    ];

    $form['advanced']['revisions']['revision_information'] = [
      '#type' => 'container',
      '#title' => $this->t('Revision information'),
      '#weight' => 20,
      '#optional' => TRUE,
    ];

    $form['advanced']['revisions']['revision_information']['revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revision'),
      '#default_value' => $new_revision_default,
      '#group' => 'revision_information',
    ];

    $form['advanced']['revisions']['revision_information']['revision_log_message'] = [
      '#title' => $this->t('Log message for the new revision'),
      '#type' => 'textarea',
      '#group' => 'revision_information',
      '#states' => [
        'visible' => [
          ':input[name="revision"]' => ['checked' => TRUE],
        ],
      ],
    ];
  }
}
