<?php

namespace Drupal\webform_revisions\Entity;

use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\webform_revisions\WebformRevisionsInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\user\UserInterface;

/**
 * Defines the webform revision entity class.
 *
 * @ContentEntityType(
 *   id = "webform_revisions",
 *   label = @Translation("Webform Revision"),
 *   label_collection = @Translation("Webform Revisions"),
 *   label_singular = @Translation("webform revision"),
 *   label_plural = @Translation("webform revisions"),
 *   label_count = @PluralTranslation(
 *     singular = "@count webform revision",
 *     plural = "@count webform revisions"
 *   ),
 *   handlers = {
 *     "access" = "Drupal\webform_revisions\WebformRevisionAccessController",
 *     "form" = {
 *       "default" = "Drupal\webform_revisions\WebformRevisionForm",
 *       "delete" = "Drupal\webform_revisions\Form\WebformRevisionDeleteForm",
 *       "edit" = "Drupal\webform_revisions\WebformRevisionForm"
 *     },
 *   },
 *   base_table = "webform_revisions",
 *   data_table = "webform_revision_field_data",
 *   revision_table = "webform_revision_revision",
 *   revision_data_table = "webform_revision_revision_data",
 *   fieldable = TRUE,
 *   field_ui_base_route = "entity.webform_revision.edit_form",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "form",
 *     "name" = "name",
 *     "uid" = "uid",
 *     "uuid" = "uuid",
 *     "published" = "published",
 *   },
 *   bundle_entity_type = "webform",
 *   permission_granularity = "entity_type",
 *   admin_permission = "administer webform_revisions",
 *   field_ui_base_route = "entity.webform_revisions.edit_form",
 *   links = {
 *     "add-page" = "/webform_revisions/add",
 *     "add-form" = "/webform_revisions/add/{webform}",
 *     "canonical" = "/webform_revisions/{webform}",
 *     "delete-form" = "/webform_revisions/{webform}/delete",
 *     "edit-form" = "/webform_revisions/{webform}/edit",
 *     "admin-form" = "/admin/structure/webform/manage/{webform_revisions_bundle}"
 *   }
 * )
 */
class WebformRevisions extends EditorialContentEntityBase implements WebformRevisionsInterface {

  use EntityChangedTrait;

  /**
   * Value that represents the webform_revisions being published.
   */
  const PUBLISHED = 1;

  /**
   * Value that represents the webform_revisions being unpublished.
   */
  const NOT_PUBLISHED = 0;

  /**
   * The ConfigEntity for which revisions are being stored.
   *
   * @var \Drupal\Core\Entity\EntityInterface;
   */
  protected $entity;

  /**
   * Constructs a Webform Revisions object.
   *
   * @param array $values
   *   An array of values to set, keyed by property name. If the entity type
   *   has bundles, the bundle key has to be specified.
   * @param string $entity_type
   *   The type of the entity to create.
   */
  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublisher() {
    return $this->get('revision_uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublisherId() {
    return $this->get('revision_uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublisherId($uid) {
    $this->set('revision_uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getType() {
    return $this->bundle->entity->getType();
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    $this->setNewRevision();
    parent::save();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['configuration'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Configuration'))
      ->setDescription(t('The serialised webform configuration for this revision.'))
      ->setReadOnly(TRUE)
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the webform was last edited.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}
