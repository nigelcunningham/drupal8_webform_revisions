<?php

namespace Drupal\webform_revisions\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform_revisions\WebformRevisionsInterface;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\webform\Entity\Webform;

/**
 * Provides a form for reverting a webform_revisions revision.
 *
 * @internal
 */
class WebformRevisionsRevertForm extends ConfirmFormBase {

  /**
   * The webform_revisions revision.
   *
   * @var \Drupal\webform_revisions\WebformRevisionsInterface
   */
  protected $revision;

  /**
   * The webform.
   *
   * @var \Drupal\webform\Entity\Webform
   */
  protected $webform;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new WebformRevisionsRevisionRevertForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $webform_revisions_storage
   *   The webform_revisions storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityStorageInterface $webform_revisions_storage, DateFormatterInterface $date_formatter, TimeInterface $time) {
    $this->webform_revisionsStorage = $webform_revisions_storage;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')->getStorage('webform_revisions'),
      $container->get('date.formatter'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_revisions_revision_revert_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to revert to the revision from %revision-date?', ['%revision-date' => $this->dateFormatter->format($this->revision->getRevisionCreationTime())]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.webform_revisions.revisions', ['webform' => $this->webform->id(), 'webform_revisions' => $this->revision->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Revert');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Webform $webform = NULL, $webform_revision = NULL) {
    $this->webform = $webform;
    $this->revision = $this->webform_revisionsStorage->loadRevision($webform_revision);
    $form = parent::buildForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // The revision timestamp will be updated when the revision is saved. Keep
    // the original one for the confirmation message.
    $original_revision_timestamp = $this->revision->getRevisionCreationTime();

    $this->revision = $this->prepareRevertedRevision($this->revision, $form_state);

    $originalLogMessage = $this->revision->getRevisionLogMessage();
    $logMessage = $originalLogMessage ?
      t('Copy of the revision from %date (%message).', [
        '%date' => $this->dateFormatter->format($original_revision_timestamp),
        '%message' => $originalLogMessage,
      ]) :
      t('Copy of the revision from %date.', [
        '%date' => $this->dateFormatter->format($original_revision_timestamp)
      ]);

    $this->revision->setRevisionLogMessage($logMessage);
    $this->revision->setRevisionUserId($this->currentUser()->id());
    $this->revision->setRevisionCreationTime($this->time->getRequestTime());
    $this->revision->setChangedTime($this->time->getRequestTime());
    $this->revision->save();

    $this->webform->setElements(JSON::decode($this->revision->get('configuration')->value));
    $this->webform->save();

    $this->logger('content')->notice('@form: reverted %title revision %revision.', ['@form' => $this->webform->label(), '%revision' => $this->revision->getRevisionId()]);
    drupal_set_message(t('Webform %title has been reverted to the revision from %revision-date.', ['%title' => $this->webform->label(), '%revision-date' => $this->dateFormatter->format($original_revision_timestamp)]));
    $form_state->setRedirect(
      'entity.webform_revisions.revisions',
      ['webform' => $this->webform->id()]
    );
  }

  /**
   * Prepares a revision to be reverted.
   *
   * @param \Drupal\webform_revisions\WebformRevisionsInterface $revision
   *   The revision to be reverted.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return \Drupal\webform_revisions\WebformRevisionsInterface
   *   The prepared revision ready to be stored.
   */
  protected function prepareRevertedRevision(WebformRevisionsInterface $revision, FormStateInterface $form_state) {
    $revision->setNewRevision();
    $revision->isDefaultRevision(TRUE);

    return $revision;
  }

}
