<?php

namespace Drupal\webform_revisions\Controller;

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform_revisions\WebformRevisionsInterface;
use Drupal\webform_revisions\WebformRevisionStorageInterface;
use Drupal\webform_revisions\Entity\WebformRevisions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\webform\Entity\Webform;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Controller to make library functions available to various consumers.
 */
class WebformRevisionsController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a WebformRevisionsController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(DateFormatterInterface $date_formatter, RendererInterface $renderer) {
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Get a webform_revisions entity that matches a webform.
   */
  public function revisionsEntityFromWebform(Webform $webform) {
    $id = $webform->id();
  }

  /**
   * Generates an overview table of older revisions of a webform.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   A webform object.
   *
   * @return array
   *   An array as expected by \Drupal\Core\Render\RendererInterface::render().
   */
  public function revisionsForm(Webform $webform) {

    // @todo I plan on making this function more generic once webforms are working.
    $configEntity = $webform;
    $configEntityID = $configEntity->id();
    $configEntityType = $configEntity->getEntityType()->id();

    $account = $this->currentUser();

    $contentEntityID = $configEntity->getThirdPartySetting('webform_revisions', 'contentEntity_id');

    if (!$contentEntityID) {
      $build['#title'] = $this->t('No revisions for %title', ['%title' => $configEntity->label()]);

      $build['message'] = [
        '#theme' => 'markup',
        '#markup' => "No revisions have been created for this webform yet. There's just the original form.",
      ];

      return $build;
    }

    // First, get the corresponding webform_revisions entity, if it exists.
    $revisionsEntityStorage = \Drupal::entityTypeManager()
      ->getStorage('webform_revisions');

    $revisionsEntity = $revisionsEntityStorage->load($contentEntityID);

    $build['#title'] = $this->t('Revisions for %title', ['%title' => $configEntity->label()]);

    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert $configEntityType revisions") || $account->hasPermission('revert all revisions') || $account->hasPermission('administer nodes')) && $configEntity->access('update'));
    $delete_permission = (($account->hasPermission("delete $configEntityType revisions") || $account->hasPermission('delete all revisions') || $account->hasPermission('administer nodes')) && $configEntity->access('delete'));

    $rows = [];

    foreach ($this->getRevisionIds($revisionsEntity, $revisionsEntityStorage) as $vid) {
      /** @var \Drupal\node\NodeInterface $revision */
      $revision = $revisionsEntityStorage->loadRevision($vid);

      $username = [
        '#theme' => 'username',
        '#account' => $revision->getRevisionUser(),
      ];

      // Use revision link to link to revisions that are not active.
      $date = $this->dateFormatter->format($revision->getRevisionCreationTime(), 'short');

      // We treat also the latest translation-affecting revision as current
      // revision, if it was the default revision, as its values for the
      // current language will be the same of the current default revision in
      // this case.
      if ($revision->isDefaultRevision()) {
        $link = $configEntity->link($date);
      }
      else {
        $link = $this->l($date, new Url("entity.{$configEntityType}.revision", [
          $configEntityType => $configEntity->id(),
          "{$configEntityType}_revision" => $vid,
        ]));
      }

      $row = [];
      $column = [
        'data' => [
          '#type' => 'inline_template',
          '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
          '#context' => [
            'date' => $link,
            'username' => $this->renderer->renderPlain($username),
            'message' => [
              '#markup' => $revision->getRevisionLogMessage(),
              '#allowed_tags' => Xss::getHtmlTagList(),
            ],
          ],
        ],
      ];
      // @todo Simplify once https://www.drupal.org/node/2334319 lands.
      $this->renderer->addCacheableDependency($column['data'], $username);
      $row[] = $column;

      if ($revision->isDefaultRevision()) {
        $row[] = [
          'data' => [
            '#prefix' => '<em>',
            '#markup' => $this->t('Current revision'),
            '#suffix' => '</em>',
          ],
        ];

        $rows[] = [
          'data' => $row,
          'class' => ['revision-current'],
        ];
      }
      else {
        $links = [];
        if ($revert_permission) {
          $links['revert'] = [
            'title' => $vid < $revisionsEntity->getRevisionId() ? $this->t('Revert') : $this->t('Set as current revision'),
            'url' => Url::fromRoute("entity.{$configEntityType}.revision_revert_confirm", [
              $configEntityType => $configEntity->id(),
              "{$configEntityType}_revision" => $vid,
            ]),
          ];
        }

        if ($delete_permission) {
          $links['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute("entity.{$configEntityType}.revision_delete_confirm", [
              $configEntityType => $configEntity->id(),
              "{$configEntityType}_revision" => $vid,
            ]),
          ];
        }

        $row[] = [
          'data' => [
            '#type' => 'operations',
            '#links' => $links,
          ],
        ];

        $rows[] = $row;
      }

    }

    $build['webform_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
      '#attached' => [
        'library' => ['node/drupal.node.admin'],
      ],
      '#attributes' => ['class' => 'node-revision-table'],
    ];

    $build['pager'] = ['#type' => 'pager'];

    return $build;
  }

  /**
   * Create revision when a new webform version is saved.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform
   */
  static public function presave(Webform $webform) {

    if (!$webform->get('revision')) {
      return;
    }

    $contentID = $webform->getThirdPartySetting(
      'webform_revisions', 'contentEntity_id');

    if (!$contentID) {
      /**
       * Make a webform_revisions entity using either the previous version of
       * the webform or (failing that) the current version.
       * We're doing this here rather than in the update hook because we want
       * to save the reference to the entity in the third party settings of the
       * webform that is being saved now.
       */
      $originalEntity = \Drupal::entityTypeManager()
        ->getStorage('webform')
        ->load($webform->id());

      $source = $originalEntity ? $originalEntity : $webform;

      $container = \Drupal::getContainer();

      $contentEntity = \Drupal::entityTypeManager()
        ->getStorage('webform_revisions')
        ->create([
          'form' => $source->get('uuid'),
          'configuration' => JSON::encode($source->getElementsDecoded()),
          'revision_uid' => $container->get('current_user')->id(),
          'revision_creation_time' => $container->get('datetime.time')->getRequestTime(),
          'revision_log_message' => $webform->get('revision_log_message'),
        ]);

      $contentEntity->save();
      $contentID = $contentEntity->id();

      $webform->setThirdPartySetting(
        'webform_revisions', 'contentEntity_id', $contentID);

      $webform->set('revision', FALSE);
    }
  }

  /**
   * Create revision when a new webform version is saved.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform
   */
  static public function createRevision(Webform $webform) {

    if (!$webform->get('revision')) {
      return;
    }

    $container = \Drupal::getContainer();

    $contentID = $webform->getThirdPartySetting(
      'webform_revisions', 'contentEntity_id');

    $revisionsEntityStorage = \Drupal::entityTypeManager()
      ->getStorage('webform_revisions');

    $revisionsEntity = $revisionsEntityStorage->load($contentID);
    $revisionsEntity->set('configuration', JSON::encode($webform->getElementsDecoded()));
    $revisionsEntity->setRevisionUserId($container->get('current_user')->id());
    $revisionsEntity->setRevisionCreationTime($container->get('datetime.time')->getRequestTime());
    $revisionsEntity->setRevisionLogMessage($webform->get('revision_log_message'));
    $revisionsEntity->setNewRevision(TRUE);
    $revisionsEntity->save();

  }

  /**
   * Create revision when a new webform version is saved.
   *
   * @param \Drupal\webform\Entity\Webform $webform
   *   The webform
   */
  static public function deleteRevisions(Webform $webform) {

    $contentID = $webform->getThirdPartySetting(
      'webform_revisions', 'contentEntity_id');

    if (!$contentID) {
      return;
    }

    $revisionsEntityStorage = \Drupal::entityTypeManager()
      ->getStorage('webform_revisions')
      ->delete([$contentID]);
  }

  /**
   * Gets a list of revision IDs for a specific webform.
   *
   * @param \Drupal\webform_revisions\WebformRevisionsInterface $webformRevisions
   *   The webform revisions entity.
   * @param \Drupal\webform_revisions\WebformRevisionStorageInterface $webform_storage
   *   The webform storage handler.
   *
   * @return int[]
   *   Node revision IDs (in descending order).
   */
  protected function getRevisionIds(WebformRevisionsInterface $webform, SqlContentEntityStorage $storage) {
    $result = $storage->getQuery()
      ->allRevisions()
      ->condition($webform->getEntityType()->getKey('id'), $webform->id())
      ->sort($webform->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    return array_keys($result);
  }

  /**
   *
   */
  public static function revisionShow(Webform $webform, int $webform_revision) {
    $revisionsEntityStorage = \Drupal::entityTypeManager()
      ->getStorage('webform_revisions');
    $revision = $revisionsEntityStorage->loadRevision($webform_revision);

    $build['revision'] = [
      '#theme' => 'markup',
      '#markup' => '<pre>' . print_r(JSON::decode($revision->get('configuration')->value), TRUE) . '</pre>',
    ];
    return $build;
  }

  /**
   *
   */
  public static function revisionPageTitle(Webform $webform, int $webform_revision) {
    return '"' . $webform->get('title') . '" webform, revision ' . $webform_revision;
  }

}
